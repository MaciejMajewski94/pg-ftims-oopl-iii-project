
package cashmachiner;

/**
 *
 * @author Majson
 */
import java.util.ArrayList;


public class Cashmachine {
    ArrayList<Customer> customers = new ArrayList<Customer>();
    
    void addCustomer(Customer customer) {
        customers.add(customer);
    }

    Customer getCustomer(int account) {
        return customers.get(account);
    }
    
    ArrayList<Customer> getCustomers(){
        return customers;
    }
    
}