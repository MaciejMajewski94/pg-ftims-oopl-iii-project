
package cashmachiner;

/**
 *
 * @author Majson
 */
class InvalidAccountTypeException extends Exception {

    public InvalidAccountTypeException() {
        super("Invalid Account Type Selected");
    }
    
}